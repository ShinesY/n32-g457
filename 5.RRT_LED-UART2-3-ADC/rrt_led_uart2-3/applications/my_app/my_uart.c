/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-12     远小刘       the first version
 */
#include <my_uart.h>
#include <rtthread.h>
#include <rtdevice.h>



#define sample_uart_name "uart2"

rt_device_t serial ;//串口句柄
struct rt_semaphore rx_sem;//信号量



/**
  * @brief  uart_input //接收数据回调函数
  * @param  dev
  *         size
  * @retval RT_EOK
  */
static rt_err_t uart_input(rt_device_t dev, rt_size_t size)
{
    // 串口接收到数据后产生中断，调用此回调函数，然后发送接收信号量
    rt_sem_release(&rx_sem);

    return RT_EOK;
}

void serial_thread_entry(void *parameter)
{
    char ch;

    while (1)
    {
        /* 从串口读取一个字节的数据，没有读取到则等待接收信号量 */
        while (rt_device_read(serial, -1, &ch, 1) != 1)
        {
            /* 阻塞等待接收信号量，等到信号量后再次读取数据 */
            rt_sem_take(&rx_sem, RT_WAITING_FOREVER);
        }
        ch = ch + 0;

        rt_device_write(serial, 0, &ch, 1);
        /* 读取到的数据输出 */
        rt_kprintf("%c",ch);
        rt_thread_mdelay(50);
    }
}



int uart2_sample(void)
{

    rt_err_t ret = RT_EOK;
    serial = rt_device_find(sample_uart_name);
    if (serial != RT_NULL) {
        rt_kprintf("find failed\n");
        return RT_ERROR;
    }
    // 初始化信号量  0个信号量，先进先出RT_IPC_FLAG_FIFO，
    rt_sem_init(&rx_sem, "rx_sem", 0, RT_IPC_FLAG_FIFO);
    // 以中断接收及轮询发送模式打开串口设备
    rt_device_open(serial, RT_DEVICE_FLAG_INT_RX);
    // 设置接收回调函数
    rt_device_set_rx_indicate(serial, uart_input);
    //创建 serial 线程
    rt_thread_t thread = rt_thread_create("serial", serial_thread_entry, RT_NULL, 1024, 25, 10);
    // 创建成功则启动线程
    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);
    }
    else
    {
        ret = RT_ERROR;
    }

    return ret;
}




