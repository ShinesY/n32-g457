/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-16     BOLU       the first version
 */
#include "my_adc.h"
#include <rtthread.h>
#include <rtdevice.h>


#define ADC_DEV_NAME "adc1"             //ADC设备名称
#define ADC_DEV_CHANNEL 6               //ADC通道
rt_adc_device_t adc_dev;                // ADC 设备句柄
#define REFER_VOLTAGE       330         // 参考电压 3.3V,数据精度乘以100保留2位小数
#define CONVERT_BITS        (1 << 12)   // 转换位数为12位

rt_uint32_t adc_vol(void)
{
        rt_uint32_t value = 0, vol = 0;
        rt_err_t ret;

        /* 读取采样值 */
        value = rt_adc_read(adc_dev, ADC_DEV_CHANNEL);
//        rt_kprintf("the value is :%d \n", value);//

        /* 转换为对应电压值 */
        vol = value * REFER_VOLTAGE / CONVERT_BITS;
//        rt_kprintf("the voltage is :%d\n", vol);

        return vol;
}
/*
 * ADC线程
 *
 */
/*void ADC1_thread4_entry(void *parameter)
{
    rt_uint32_t Val = 0,i=0;
//    Val  = adc_vol();
    while(1)
    {
        Val  = adc_vol();
        if (Val > 300)
        {
            state_flag = 1;
            i=1;
            rt_kprintf("the voltage is :%d\n", Val);
        }
        rt_thread_mdelay(50);
    }
}*/

/*
 * ADC初始化

 * */
int adc_Pc0init(void)
{
    //查找ADC设备
    adc_dev = (rt_adc_device_t) rt_device_find(ADC_DEV_NAME);
    if (adc_dev != RT_NULL)
    {
        rt_kprintf("ADC find successful\n");
    }
    else
    {
        rt_kprintf("ADC find fail\n");
        return -RT_ERROR;
    }
    //使能adc设备
    rt_adc_enable(adc_dev, ADC_DEV_CHANNEL);

    /* 创建 serial 线程 */
/*    rt_thread_t thread4 = rt_thread_create("ADC1_PC0", ADC1_thread4_entry, RT_NULL, 1024, 22, 10);
    // 创建成功则启动线程
    if (thread4 != RT_NULL)
    {
        rt_thread_startup(thread4);
        rt_kprintf("ADC thread4 successful\n");
    }
    else
    {
        rt_kprintf("ADC thread4 fail\n");
        return 0;
    }*/
    return RT_EOK;
}














