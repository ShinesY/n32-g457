/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-15     BOLU       the first version
 */
#include "my_uart3.h"

#include <rtthread.h>
#include <rtdevice.h>


#define SAMPLE_UART_NAME    "uart3"

static rt_device_t serial3;//串口3句柄

struct serial_configure config = RT_SERIAL_CONFIG_DEFAULT;//配置uart3初始化配置参数

static struct rt_messagequeue rx_mq;// 消息队列控制块

static char msg_pool[256];              //消息队列数据存储区


struct rx_msg//串口接收消息结构
{
    rt_device_t dev;
    rt_size_t size;
};

// 接收数据回调函数
//dev 句柄
//size : 接收数据的长度
static rt_err_t uart_input(rt_device_t dev, rt_size_t size)
{
    struct rx_msg msg;
    rt_err_t result;
    msg.dev = dev;
    msg.size = size;

    result = rt_mq_send(&rx_mq, &msg, sizeof(msg));//发送串口接收到的信息，并返回数据长度
    if ( result == -RT_EFULL)
    {
        /* 消息队列满 */
        rt_kprintf("message queue full！\n");
    }
    return result;
}


/*
  *    接收dma发送的消息队列
 *  buffer ：数据存储
 */
rt_uint8_t uart3_dma_receive(rt_uint8_t *buffer)
{
    struct rx_msg msg1;
    rt_err_t result;
    rt_uint8_t length = RT_NULL;


    rt_memset(&msg1, 0, sizeof(msg1));
    /* 从消息队列中读取消息，没读到则阻塞300ms*/
    result = rt_mq_recv(&rx_mq, &msg1, sizeof(msg1), RT_WAITING_FOREVER);//看系统tick时间【这里系统时间换算为是以1ms为单位】//接收消息队列发送的数据
    if (result == RT_EOK)
    {
        /* 从串口读取数据*/
        length = rt_device_read(msg1.dev, 0, buffer, msg1.size);
        /* 打印数据 */

        for (int var = 0; var < length; ++var)
        {
            rt_kprintf("%c", buffer[var]);
        }
 //       rt_kprintf("length = %d\n", length);
        rt_kprintf("\n ");
        return length;
    }
    return 0;
}




/***
 * uart3_dma线程
 *
 *
 * */


static void uart3_thread_entry(void *parameter)
{
    rt_uint8_t rx_length;
    static char rx_buffer[256];

    while (1)
    {
        rx_length =  uart3_dma_receive(rx_buffer);
        if (rx_length > 0) {
            rt_kprintf("rx_dma = %d\n",rx_length);
        }

        rt_thread_mdelay(50);
    }
}

int uart3_dam_sample(void)
{
    //查找串口设备
    serial3 = rt_device_find(SAMPLE_UART_NAME);
    if (serial3 == RT_NULL)
    {
        rt_kprintf("uart3 find fail\n");
        return 0;
    }
    else
    {
        rt_kprintf("uart3 find fail successful\n");
    }
    //创建消息队列-静态时效性
    rt_mq_init(&rx_mq, "rx_mq",
                   msg_pool,                 /* 存放消息的缓冲区 */
                   sizeof(struct rx_msg),    /* 一条消息的最大长度 */
                   sizeof(msg_pool),         /* 存放消息的缓冲区大小 */
                   RT_IPC_FLAG_FIFO);        /* 如果有多个线程等待，按照先来先得到的方法分配消息 */
    // 修改串口配置参数
    config.baud_rate = BAUD_RATE_115200;        //修改波特率为 9600
    config.data_bits = DATA_BITS_8;           //数据位 8
    config.stop_bits = STOP_BITS_1;           //停止位 1
    config.bufsz = 128;                   //修改缓冲区 buff size 为 128
    config.parity = PARITY_NONE;           //无奇偶校验位


    rt_device_control(serial3, RT_DEVICE_CTRL_CONFIG, &config);//控制串口设备。通过控制接口传入命令控制字，与控制参数


    rt_device_open(serial3, RT_DEVICE_FLAG_DMA_RX);// 以 DMA 接收及轮询发送方式打开串口设备

    rt_device_set_rx_indicate(serial3, uart_input);// 设置接收回调函数

    /* 创建 serial 线程 */
    rt_thread_t thread3 = rt_thread_create("uart3_dma", uart3_thread_entry, RT_NULL, 1024, 25, 10);
    /* 创建成功则启动线程 */
    if (thread3 != RT_NULL)
    {
        rt_thread_startup(thread3);
        rt_kprintf("uart3 thread3 successful\n");
    }
    else
    {
        rt_kprintf("thread3 fail\n");
        return 0;
    }
}












