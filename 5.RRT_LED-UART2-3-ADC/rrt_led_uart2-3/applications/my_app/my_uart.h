/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-12     ԶС��       the first version
 */
#ifndef APPLICATIONS_MY_APP_MY_UART_H_
#define APPLICATIONS_MY_APP_MY_UART_H_

int uart2_sample(void);

#endif /* APPLICATIONS_MY_APP_MY_UART_H_ */
