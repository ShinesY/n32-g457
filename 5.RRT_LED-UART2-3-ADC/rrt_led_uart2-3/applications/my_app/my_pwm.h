/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-09     BOLU       the first version
 */
#ifndef APPLICATIONS_MY_APP_MY_PWM_H_
#define APPLICATIONS_MY_APP_MY_PWM_H_

int pwm_init(void);

#endif /* APPLICATIONS_MY_APP_MY_PWM_H_ */
